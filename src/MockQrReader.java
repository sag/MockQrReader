import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.itextpdf.text.exceptions.UnsupportedPdfException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;

public class MockQrReader {

	public static void main(String[] args) {
		try {
			Set<String> oQRs = leerPDF(new FileInputStream("qrcode.pdf"));

			System.out.println(oQRs);

			for (String qr : oQRs) {

				System.out.println(decodeAFIPQR(qr));

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Set<String> leerPDF(InputStream oIn) throws Exception {
		PdfReader oReader = new PdfReader(oIn);
		PdfReaderContentParser oParser = new PdfReaderContentParser(oReader);
		ImageRenderListener oListener = new ImageRenderListener();
		for (int i = 1; i <= oReader.getNumberOfPages(); i++) {
			oParser.processContent(i, oListener);
		}
		return oListener.getQRS();
	}

	public static String leerImagen(BufferedImage oImage) throws Exception {
		BufferedImageLuminanceSource oSource = new BufferedImageLuminanceSource(oImage);
		HybridBinarizer oBina = new HybridBinarizer(oSource);
		BinaryBitmap oBitmap = new BinaryBitmap(oBina);
		MultiFormatReader oReader = new MultiFormatReader();
		return oReader.decode(oBitmap).getText();
	}

	public static Map<String, Object> decodeAFIPQR(String sURL) throws Exception {
		Map<String, String> oParam = _splitQuery(new URL(sURL));
		if (oParam.containsKey("p")) {
			byte[] oBytes = Base64.getDecoder().decode(oParam.get("p"));
			return _jsonToMap(new JSONObject(new String(oBytes)));
		}
		return null;
	}

	private static Map<String, String> _splitQuery(URL oURL) throws Exception {
		Map<String, String> oParam = new LinkedHashMap<String, String>();
		String oQuery = oURL.getQuery();
		String[] oSplit = oQuery.split("&");
		for (String oAux : oSplit) {
			int i = oAux.indexOf("=");
			oParam.put(URLDecoder.decode(oAux.substring(0, i), "UTF-8"),
					URLDecoder.decode(oAux.substring(i + 1), "UTF-8"));
		}
		return oParam;
	}

	private static Map<String, Object> _jsonToMap(JSONObject json) throws JSONException {
		Map<String, Object> retMap = new HashMap<String, Object>();
		if (json != JSONObject.NULL) {
			retMap = _toMap(json);
		}
		return retMap;
	}

	private static Map<String, Object> _toMap(JSONObject object) throws JSONException {
		Map<String, Object> map = new HashMap<String, Object>();
		Iterator<String> keysItr = object.keys();
		while (keysItr.hasNext()) {
			String key = keysItr.next();
			Object value = object.get(key);
			if (value instanceof JSONArray) {
				value = _toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = _toMap((JSONObject) value);
			}
			map.put(key, value);
		}
		return map;
	}

	private static List<Object> _toList(JSONArray array) throws JSONException {
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < array.length(); i++) {
			Object value = array.get(i);
			if (value instanceof JSONArray) {
				value = _toList((JSONArray) value);
			} else if (value instanceof JSONObject) {
				value = _toMap((JSONObject) value);
			}
			list.add(value);
		}
		return list;
	}

	private static class ImageRenderListener implements RenderListener {

		// Solo obtiene los diferentes
		private Set<String> qrs = new HashSet<>();

		public ImageRenderListener() {
		}

		public void beginTextBlock() {
		}

		public void renderText(TextRenderInfo oInfo) {
		}

		public void endTextBlock() {
		}

		public Set<String> getQRS() {
			return qrs;
		}

		public void renderImage(ImageRenderInfo oInfo) {
			try {
				qrs.add(leerImagen(oInfo.getImage().getBufferedImage()));
			} catch (final UnsupportedPdfException e) {
				// Con algunas imagenes tira The color depth 4 is not supported.
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}